import { Weather } from "./classes/weather";

const city = document.getElementById("city");
const forecast = document.getElementById("forecast");
const icon = document.getElementById("icon");
const temp = document.getElementById("temp");

const load = async () => {
  const weather = new Weather("Rome", "IT-62", "IT");
  const data = await weather.update();

  city.innerText = `${data.city.name} - ${data.description}`;

  forecast.innerHTML = `
    ${data.temperature.current}°C (${data.temperature.feelsLike} percepiti)
    Min: ${data.temperature.min} - Max: ${data.temperature.max}
    `;

  icon.src = data.iconUrl;

  console.log(data);
};

load();
