export class Wind {
  constructor(speed, gust, degrees) {
    this.degrees = degrees;
    this.speed = speed;
    this.gust = gust;
  }
}
