export class Temperature {
  constructor(current, min, max, feelsLike) {
    this.current = current;
    this.min = min;
    this.max = max;
    this.feelsLike = feelsLike;
  }
}
