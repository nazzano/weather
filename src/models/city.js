export class City {
  /**
   * @param {string} name City name
   * @param {string} country Country Code (i.e. IT)
   * @param {number} lat Latitude
   * @param {number} lon Longitude
   * @param {number} tz Timezone: minutes from the GMT+0
   * @param {number} sunrise Sunrise timestamp
   * @param {number} sunset Sunset timestamp
   */
  constructor(name, country, lat, lon, tz, sunrise = 0, sunset = 0) {
    this.country = country;
    this.lat = lat;
    this.lon = lon;
    this.tz = tz;
    this.sunrise = sunrise;
    this.sunset = sunset;
    this.name = name;
  }
}
